package org.filestore.api;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;

public abstract class FileItem implements Serializable {
    @XmlAttribute
    private static final long serialVersionUID = 1L;
    
    @XmlAttribute
    public abstract String getId();
    
    @XmlAttribute
    public abstract String getName();
    
    @XmlAttribute
    public abstract String getType();
    
    @XmlAttribute
    public abstract long getLength();
    
    @XmlAttribute
    public abstract long getNbdownloads();
    
    @XmlAttribute
    public abstract String getOwner();
    
    public abstract List<String> getReceivers();
    
    @XmlAttribute
    public abstract String getMessage();
    
    @XmlAttribute
    public abstract Date getCreation();
    
    @XmlAttribute
    public abstract Date getLastdownload();
    
}

